import {
  Box,
  Flex,
  Heading,
  Icon,
  Link,
  Image,
  Menu,
  MenuButton,
  MenuItem,
  MenuList,
  Button,
} from "@chakra-ui/react"
import { HiOutlineMenuAlt3, HiShoppingBag, HiUser } from "react-icons/hi"
import { useState } from "react"
import { Link as RouterLink, useLocation, useNavigate } from "react-router-dom"
import HeaderMenuItem from "./HeaderMenuItem"
import { useDispatch, useSelector } from "react-redux"
import { logout } from "../actions/userActions"
import { IoChevronDown } from "react-icons/io5"
import { MdShoppingBag } from "react-icons/md"

const Header = () => {
  const state = useSelector((state) => state)
  console.log(state)
  const dispatch = useDispatch()
  const navigate = useNavigate()
  const location = useLocation()

  const isHomeScreen = location.pathname === "/"

  const [showNav, setShowNav] = useState(false)
  const [showCat, setShowCat] = useState(false)

  const userLogin = useSelector((state) => state.userLogin)
  const { userInfo } = userLogin

  const logoutHandler = () => {
    dispatch(logout())
    navigate("/login")
  }

  return (
    <Flex
      as="header"
      align="center"
      justifyContent="center"
      wrap="wrap"
      py="6"
      px="6"
      bgColor="white"
      w="100%"
      pos="fixed"
      top="0"
      left="0"
      zIndex={9999}
      boxShadow="lg"
    >
      <Flex
        direction="row"
        alignItems="center"
        gap="5"
        w="90%"
        justifyContent="space-between"
      >
        <Link as={RouterLink} to="/">
          {/* <Heading
            as="h1"
            color="gray.800"
            size="md"
            letterSpacing="wide"
            fontWeight="bold"
            fontSize="2xl"
          >
            ShoeVista
          </Heading> */}

          <Image src="/logo/ShoeVista3.jpg" h="10" />
        </Link>

        <Box
          display={{ base: "block", md: "none" }}
          onClick={() => setShowNav(!showNav)}
        >
          <Icon as={HiOutlineMenuAlt3} color="white" w="4" h="4" />
        </Box>

        <Box
          display={{ base: showNav ? " block" : "none", md: "flex" }}
          w={{ base: "full", md: "auto" }}
          mt={{ base: "3", md: "0" }}
        >
          <HeaderMenuItem
            url="/cart"
            label="Cart"
            icon={<Icon as={MdShoppingBag} w="6" h="6" />}
          />

          {userInfo ? (
            <Menu>
              <MenuButton
                as={Button}
                rightIcon={<IoChevronDown />}
                _hover={{ textDecor: "none", opacity: "0.7" }}
              >
                {userInfo.name}
              </MenuButton>
              <MenuList>
                <MenuItem as={RouterLink} to="/profile">
                  Profile
                </MenuItem>
                <MenuItem onClick={logoutHandler}>Logout</MenuItem>
              </MenuList>
            </Menu>
          ) : (
            <HeaderMenuItem
              url="/login"
              label="Login"
              icon={<Icon as={HiUser} w="6" h="6" />}
            />
          )}

          {/* Admin Menu */}
          {userInfo && userInfo.isAdmin && (
            <Menu>
              <MenuButton
                ml="3"
                fontSize="sm"
                fontWeight="semibold"
                as={Button}
                textTransform="uppercase"
                _hover={{ textDecor: "none", opacity: "0.7" }}
              >
                Manage <Icon as={IoChevronDown} />
              </MenuButton>
              <MenuList>
                <MenuItem as={RouterLink} to="/admin/users">
                  All Users
                </MenuItem>
                <MenuItem as={RouterLink} to="/admin/productlist">
                  All Products
                </MenuItem>
                <MenuItem as={RouterLink} to="/admin/orderlist">
                  All Orders
                </MenuItem>
              </MenuList>
            </Menu>
          )}
        </Box>
      </Flex>
    </Flex>
  )
}

export default Header
