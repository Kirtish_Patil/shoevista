import React from "react"
import { Flex, Text } from "@chakra-ui/react"

const Footer = () => {
  return (
    <Flex as='footer' justifyContent='center' py='5' borderTop='1px' borderColor='white' bgColor='customBlue.100' >
      <Text color='black' py='6'>
        Copyright {new Date().getFullYear()}. ShoeVista. All Rights Reserved.
      </Text>
    </Flex>
  )
}

export default Footer
