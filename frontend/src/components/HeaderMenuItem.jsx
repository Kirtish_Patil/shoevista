import { Link } from "@chakra-ui/react"
import { Link as RouterLink } from "react-router-dom"

import React from "react"

const HeaderMenuItem = ({ url, label, icon }) => {
  return (
    <Link
      as={RouterLink}
      to={url}
      fontSize="sm"
      letterSpacing="wide"
      textTransform="uppercase"
      mr="5"
      display="flex"
      alignItems="center"
      color="black"
      mb={{ base: "3", md: 0 }}
      mt={{base: '4', md: 0}}
      gap='1'
      fontWeight="bold"
      _hover={{textDecor: 'none', backgroundColor: 'customBlue.200', color: 'white',}}
      px='4'
      py='2'
      borderRadius='10px'
      transition='background-color 0.3s'
    >
      {icon}
      {/* {label} */}
    </Link>
  )
}

export default HeaderMenuItem
