import { Box, Flex, Heading, Image, Link, Text } from "@chakra-ui/react"
import { Link as RouterLink } from "react-router-dom"
import Rating from "./Rating"

const ProductCard = ({ product, cat }) => {
  return (
    <Link
      as={RouterLink}
      to={`/product/${product._id}`}
      _hover={{ textDecor: "none", transform: 'scale(1.05)', backgroundColor: 'white', border: '1px', borderColor: 'customBlue.200' }}
      bgColor="gray.100"
      borderRadius='lg'
    >
      <Box border='1px' borderColor='white' borderRadius='lg' > 
        <Image
          src={product.image}
          alt={product.name}
          w="full"
          objectFit="cover"
          borderRadius='lg'
        />
        <Flex py="5" px="4" direction="column" justifyContent="space-between">
          <Heading as="h4" fontSize="lg" mb="3" color="black">
            {product.name}
          </Heading>
          <Flex alignItems="center" justifyContent="space-between">
            <Rating value={product.rating} />
            <Text color="black">${product.price}</Text>
          </Flex>
        </Flex>
      </Box>
    </Link>
  )
}

export default ProductCard
