import { Alert, AlertIcon, AlertTitle } from "@chakra-ui/react"

import React from "react"

const Message = ({ type = "info", children }) => {
  return (
    <Alert
      status={type}
      variant='left-accent'
    >
      <AlertIcon />
      <AlertTitle fontSize="lg">
        {children}
      </AlertTitle>
    </Alert>
  )
}
export default Message
