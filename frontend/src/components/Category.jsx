import React from "react"
import { BreadcrumbItem, Text, useColorModeValue } from "@chakra-ui/react"
import Scroll from "react-scroll-to-element"

const Category = ({ element, text }) => {
  // const hoverBgColor = useColorModeValue("gray.200, gray.800")

  return (
    <BreadcrumbItem>
      <Scroll type="id" element={element} offset={-144}>
        <Text
          _hover={{ color: "white", backgroundColor: 'customBlue.200' }}
          fontWeight="semibold"
          backgroundColor='white'
          color='black'
          fontSize="sm"
          borderRadius='md'
          px='4'
          py='2'
          transition='background-color 0.3s, color 0.3s'
        >
          {text}
        </Text>
      </Scroll>
    </BreadcrumbItem>
  )
}

export default Category
