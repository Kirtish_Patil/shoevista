import React from "react"
import {
  Breadcrumb,
  BreadcrumbItem,
  BreadcrumbLink,
  Flex,
} from "@chakra-ui/react"
import { IoCaretForwardSharp } from "react-icons/io5"
import { Link as RouterLink } from "react-router-dom"

const CheckoutSteps = ({ step1, step2, step3, step4 }) => {
  return (
    <Flex justifyContent="center" mb="8">
      <Breadcrumb color="customBlue.600" separator={<IoCaretForwardSharp />}>
        {/* Step 1 */}
        <BreadcrumbItem>
          {step1 ? (
            <BreadcrumbLink
              as={RouterLink}
              to="/login?redirect=/shipping"
              fontWeight="medium"
            >
              Login
            </BreadcrumbLink>
          ) : (
            <BreadcrumbLink _disabled color="gray.400">
              Login
            </BreadcrumbLink>
          )}
        </BreadcrumbItem>

        {/* Step 2 */}
        <BreadcrumbItem>
          {step2 ? (
            <BreadcrumbLink
              as={RouterLink}
              to="/shipping"
              fontWeight="medium"
            >
              Shipping
            </BreadcrumbLink>
          ) : (
            <BreadcrumbLink _disabled color="gray.400" fontWeight="medium">
              Shipping
            </BreadcrumbLink>
          )}
        </BreadcrumbItem>

        {/* Step 3 */}
        <BreadcrumbItem>
          {step3 ? (
            <BreadcrumbLink as={RouterLink} to="/payment" fontWeight="medium">
              Payment
            </BreadcrumbLink>
          ) : (
            <BreadcrumbLink _disabled color="gray.400" fontWeight="medium">
              Payment
            </BreadcrumbLink>
          )}
        </BreadcrumbItem>

        {/* Step 4 */}
        <BreadcrumbItem>
          {step4 ? (
            <BreadcrumbLink
              as={RouterLink}
              to="/placeorder"
              fontWeight="medium"
            >
              Place Order
            </BreadcrumbLink>
          ) : (
            <BreadcrumbLink _disabled color="gray.400" fontWeight="medium">
              Place Order
            </BreadcrumbLink>
          )}
        </BreadcrumbItem>
      </Breadcrumb>
    </Flex>
  )
}

export default CheckoutSteps
