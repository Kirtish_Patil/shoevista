import { Flex, Icon, Text } from "@chakra-ui/react"
import { BsStarFill, BsStarHalf, BsStar } from "react-icons/bs"

const Rating = ({ value, text }) => {
  return (
    <Flex align='center' > 
      <Icon as={value >= 1 ? BsStarFill : value >= 0.5 ? BsStarHalf : BsStar} color='yellow.600' />
      <Icon as={value >= 2 ? BsStarFill : value >= 1.5 ? BsStarHalf : BsStar} color='yellow.600' />
      <Icon as={value >= 3 ? BsStarFill : value >= 2.5 ? BsStarHalf : BsStar} color='yellow.600' />
      <Icon as={value >= 4 ? BsStarFill : value >= 3.5 ? BsStarHalf : BsStar} color='yellow.600' />
      <Icon as={value >= 5 ? BsStarFill : value >= 4.5 ? BsStarHalf : BsStar} color='yellow.600' />
      <Text ml='2'>{text}</Text>
    </Flex>
  )
}

export default Rating
