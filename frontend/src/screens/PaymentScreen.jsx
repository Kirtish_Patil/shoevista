import React from "react"
import {
  Button,
  Flex,
  FormControl,
  FormLabel,
  Heading,
  HStack,
  Radio,
  RadioGroup,
  Spacer,
} from "@chakra-ui/react"
import { useState, useEffect } from "react"
import { useDispatch, useSelector } from "react-redux"
import { useNavigate } from "react-router-dom"
import { savePaymentMethod } from "../actions/cartActions"
import CheckoutSteps from "../components/CheckoutSteps"
import FormContainer from "../components/FormContainer"

const PaymentScreen = () => {
  const dispatch = useDispatch()
  const navigate = useNavigate()

  const cart = useSelector((state) => state.cart)
  const { shippingAddress, paymentMethod } = cart

  const [paymentMethodRadio, setPaymentMethodRadio] = useState(
    paymentMethod || "paypal"
  )

  useEffect(() => {
    if (!shippingAddress) {
      navigate("shipping")
    }
  }, [navigate, shippingAddress])

  const submitHandler = (e) => {
    e.preventDefault()
    dispatch(savePaymentMethod(paymentMethodRadio))
    navigate("/placeorder")
  }
  return (
    <Flex
      w="full"
      alignItems="center"
      justifyContent="center"
      py="8rem"
      bgGradient="linear(to-br, customBlue.400, customBlue.700)"
    >
      <FormContainer>
        <CheckoutSteps step1 step2 step3 />

        <Heading as="h2" mb="8" fontSize="3xl" color="customBlue.600">
          Payment Method
        </Heading>

        <form onSubmit={submitHandler}>
          <FormControl as="fieldset">
            <FormLabel as="legend" color="customBlue.600" fontWeight='semibold'>
              Select Method
            </FormLabel>
            <RadioGroup value={paymentMethodRadio} flexDirection="column" color="customBlue.600" fontWeight='medium'>
              <Radio
                display="block"
                value="paypal"
                onChange={(e) => setPaymentMethodRadio(e.target.value)}
              >
                PayPal or Credit/Debit Card
              </Radio>

              <Spacer h="1" />

              <Radio
                display="block"
                value="paytm"
                onChange={(e) => setPaymentMethodRadio(e.target.value)}
              >
                Paytm
              </Radio>
            </RadioGroup>

            <Spacer h="3" />

            <Button
              _hover={{ backgroundColor: "customBlue.200", color: "black" }}
              type="submit"
              bgColor="customBlue.600"
              color="white"
              mt="4"
            >
              Continue
            </Button>
          </FormControl>
        </form>
      </FormContainer>
    </Flex>
  )
}

export default PaymentScreen
