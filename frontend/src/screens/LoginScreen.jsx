import {
  Flex,
  Button,
  FormControl,
  FormLabel,
  Heading,
  Input,
  Link,
  Spacer,
  Text,
  Grid,
  Box,
  Image,
} from "@chakra-ui/react"
import { useEffect, useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import {
  Link as RouterLink,
  useNavigate,
  useSearchParams,
} from "react-router-dom"
import { getUserDetails, loginUser } from "../actions/userActions"
import FormContainer from "../components/FormContainer"
import Message from "../components/Message"

const LoginScreen = () => {
  const dispatch = useDispatch()
  const navigate = useNavigate()

  const [searchParams] = useSearchParams()
  let redirect = searchParams.get("redirect") || "/"

  const [email, setEmail] = useState("")
  const [password, setPassword] = useState("")

  const userLogin = useSelector((state) => state.userLogin)
  const { loading, error, userInfo } = userLogin

  useEffect(() => {
    if (userInfo) {
      navigate(redirect)
    }
  }, [userInfo, navigate, redirect])

  const submitHandler = (e) => {
    e.preventDefault()
    dispatch(loginUser(email, password))
  }

  return (
    <Flex
      w="full"
      alignItems="center"
      justifyContent="center"
      p="6"
      py="9.5rem"
      bgGradient="linear(to-br, customBlue.400, customBlue.700)"
    >
      <Flex
        justifyContent="center"
        alignItems="center"
        direction="column"
        w="40%"
        px="7rem"
        color="white"
        gap="6"
      >
        <Heading fontWeight="normal" fontSize="5xl">
          Welcome
        </Heading>
        <Text fontWeight="light" fontSize="md" letterSpacing="wider">
          Welcome back, shoe enthusiast! Step into style and comfort by logging
          in to explore our latest collections. Walk with us.
        </Text>
      </Flex>
      <FormContainer>
        <Heading as="h1" mb="8" fontSize="3xl" color="customBlue.600">
          Login
        </Heading>

        {error && <Message type="error">{error}</Message>}

        <form onSubmit={submitHandler}>
          <FormControl id="email">
            <FormLabel
              htmlFor="email"
              color="customBlue.600"
              fontWeight="semibold"
            >
              Email address
            </FormLabel>
            <Input
              id="email"
              type="email"
              placeholder="username@domain.com"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </FormControl>

          <Spacer h="3" />

          <FormControl id="password">
            <FormLabel
              htmlFor="password"
              color="customBlue.600"
              fontWeight="semibold"
            >
              Password
            </FormLabel>
            <Input
              id="password"
              type="password"
              placeholder="************"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
          </FormControl>

          <Button
            _hover={{ backgroundColor: "customBlue.200", color: "black" }}
            type="submit"
            bgColor="customBlue.600"
            color="white"
            mt="6"
            isLoading={loading}
          >
            Login
          </Button>

          <Flex mt="6">
            <Text color="customBlue.600" fontWeight="semibold">
              New Customer{" "}
              <Link as={RouterLink} to="/register">
                Click here to register
              </Link>
            </Text>
          </Flex>
        </form>
      </FormContainer>
    </Flex>
  )
}

export default LoginScreen
