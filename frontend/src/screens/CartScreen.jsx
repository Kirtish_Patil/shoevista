import {
  Box,
  Button,
  Flex,
  Grid,
  Heading,
  Icon,
  Image,
  Link,
  Select,
  Spacer,
  Text,
} from "@chakra-ui/react"
import { LuTrash2 } from "react-icons/lu"
import { useParams, useSearchParams } from "react-router-dom"
import { useDispatch, useSelector } from "react-redux"
import { useEffect } from "react"
import { Link as RouterLink, useNavigate } from "react-router-dom"
import { addToCart, removeFromCart } from "../actions/cartActions"
import Message from "../components/Message"

const CartScreen = () => {
  const dispatch = useDispatch()
  const navigate = useNavigate()

  const { id } = useParams()

  const [searchParams] = useSearchParams()
  let qty = searchParams.get("qty")

  const cart = useSelector((state) => state.cart)
  const { cartItems } = cart

  useEffect(() => {
    if (id) {
      dispatch(addToCart(id, +qty))
    }
  }, [dispatch, id, qty])

  const removeFromCartHandler = (id) => {
    dispatch(removeFromCart(id))
  }

  const checkoutHandler = () => {
    navigate(`/login?redirect=/shipping`)
  }

  return (
    <Flex direction="column" py="4" px="6">
      <Box>
        <Heading mb="8">Shopping Cart</Heading>
        {cartItems.length === 0 ? (
          <Message>
            Your cart is empty.{" "}
            <Link as={RouterLink} to="/">
              Go Back
            </Link>
          </Message>
        ) : (
          <>
            {cartItems.map((item) => (
              <Flex justifyContent="center">
                <Grid
                  key={item.product}
                  w="80%"
                  alignItems="center"
                  justifyContent="space-between"
                  borderBottom="1px"
                  borderColor="gray.200"
                  py="4"
                  px="2"
                  rounded="lg"
                  _hover={{ bgColor: "gray.50" }}
                  templateColumns="1fr 4fr 2fr 1fr "
                >
                  {/* Product Image */}
                  <Image
                    src={item.image}
                    alt={item.name}
                    borderRadius="lg"
                    height="14"
                    width="14"
                    objectFit="cover"
                  />

                  {/* Product Name */}
                  <Text fontWeight="semibold" fontSize="lg" >
                    <Text fontSize='sm' color='gray.600'>({item.category})</Text>
                    <Link as={RouterLink} to={`/product/${item.product}`} _hover={{textDecor:'none', color: 'gray.400'}}>
                      {item.name}{' '}  
                    </Link>
                    <Text fontSize='sm' color='gray.600'>{item.brand}</Text>
                  </Text>

                  {/* Product Price */}
                  <Text fontWeight="semibold" fontSize="lg">
                    ₹{item.price}
                  </Text>

                  {/* Quantity Select Box */}
                  <Flex alignItems='center' justifyContent='center' gap='6'>
                    <Select
                      value={item.qty}
                      onChange={(e) =>
                        dispatch(addToCart(item.product, +e.target.value))
                      }
                      _hover={{backgroundColor: 'customBlue.100'}}
                      width="20"
                      variant='filled'
                    >
                      {[...Array(item.countInStock).keys()].map((i) => (
                        <option key={i + 1}>{i + 1}</option>
                      ))}
                    </Select>

                    {/* Delete Button */}

                    <Button 
                      onClick={() => removeFromCartHandler(item.product)}
                        _hover={{backgroundColor: 'red.200'}} transition='background-color 0.3s' 
                        borderRadius='2xl'
                    >
                      <Icon as={LuTrash2}   color='black' />
                    </Button>
                  </Flex>
                </Grid>
              </Flex>
            ))}

            <Spacer h="10" />

            {/* Column 2 */}
            <Flex justifyContent="center" alignItems="center">
              <Flex
                direction="row"
                bgGradient="linear(to-br, customBlue.400, customBlue.700)"
                rounded="md"
                padding="5"
                w="40%"
                justifyContent="space-between"
                alignItems="center"
              >
                <Flex direction="column"justifyContent='center' >
                  <Heading as="h2" fontSize="2xl" mb="2" color="white">
                    Subtotal (
                    {cartItems.reduce((acc, currVal) => acc + currVal.qty, 0)}{" "}
                    items)
                  </Heading>
                  <Text fontWeight="bold" fontSize="2xl" color="teal.100" >
                    ₹
                    {cartItems.reduce(
                      (acc, currVal) => acc + currVal.qty * currVal.price,
                      0
                    )}
                  </Text>
                </Flex>
                <Button
                  type="button"
                  disabled={cartItems.length === 0}
                  _hover={{backgroundColor: 'customBlue.600', color: 'white'}}
                  size="lg"
                  color="black"
                  bgColor="customBlue.100"
                  onClick={checkoutHandler}
                >
                  Proceed to checkout
                </Button>
              </Flex>
            </Flex>
          </>
        )}
      </Box>
    </Flex>
  )
}

export default CartScreen
