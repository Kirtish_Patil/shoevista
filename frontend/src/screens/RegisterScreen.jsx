import {
  Flex,
  Heading,
  FormControl,
  FormLabel,
  Input,
  Link,
  Spacer,
  Text,
  Button,
} from "@chakra-ui/react"
import { useDispatch, useSelector } from "react-redux"
import { useState, useEffect } from "react"
import {
  Link as RouterLink,
  useNavigate,
  useSearchParams,
} from "react-router-dom"
import { registerUser } from "../actions/userActions"
import FormContainer from "../components/FormContainer"
import Message from "../components/Message"

const RegisterScreen = () => {
  const dispatch = useDispatch()
  const navigate = useNavigate()

  const [searchParams] = useSearchParams()
  let redirect = searchParams.get("redirect") || "/"

  const userRegister = useSelector((state) => state.userLogin)
  const { loading, error, userInfo } = userRegister

  const [name, setName] = useState("")
  const [email, setEmail] = useState("")
  const [number, setNumber] = useState(null)
  const [password, setPassword] = useState("")
  const [confirmPassword, setConfirmPassword] = useState("")
  const [message, setMessage] = useState(null)

  useEffect(() => {
    if (userInfo) {
      navigate(redirect)
    }
  }, [dispatch, userInfo, redirect])

  const submitHandler = (e) => {
    e.preventDefault()
    if (password !== confirmPassword) {
      setMessage("Passwords do not match")
    } else {
      dispatch(registerUser(name, email, number, password))
    }
  }

  return (
    <Flex
      w="full"
      alignItems="center"
      justifyContent="center"
      py="5rem"
      bgGradient="linear(to-br, customBlue.400,  customBlue.500, customBlue.700)"
    >
      <Flex
        justifyContent="center"
        alignItems="center"
        direction="column"
        w="40%"
        px="7rem"
        color="white"
        gap="6"
      >
        <Heading fontWeight="normal" fontSize="5xl">
          Welcome
        </Heading>
        <Text fontWeight="light" fontSize="md" letterSpacing="wider">
          Welcome back, shoe enthusiast! Step into style and comfort by logging
          in to explore our latest collections. Walk with us.
        </Text>
      </Flex>

      <FormContainer>
        <Heading as="h1" mb="8" fontSize="3xl" color="customBlue.600">
          Register
        </Heading>

        {error && <Message type="error">{error}</Message>}
        {message && <Message type="error">{message}</Message>}

        <form onSubmit={submitHandler}>
          <FormControl id="name">
            <FormLabel
              htmlFor="name"
              color="customBlue.600"
              fontWeight="semibold"
            >
              Your Name
            </FormLabel>
            <Input
              id="name"
              type="text"
              placeholder="Your full name"
              value={name}
              onChange={(e) => setName(e.target.value)}
            />
          </FormControl>

          <Spacer h="3" />

          <FormControl id="email">
            <FormLabel
              htmlFor="email"
              color="customBlue.600"
              fontWeight="semibold"
            >
              Email address
            </FormLabel>
            <Input
              id="email"
              type="email"
              placeholder="username@domain.com"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </FormControl>

          <Spacer h="3" />

          <FormControl id="number">
            <FormLabel
              htmlFor="number"
              color="customBlue.600"
              fontWeight="semibold"
            >
              Phone Number
            </FormLabel>
            <Input
              id="number"
              type="number"
              placeholder="add a valid 10 digit phone number"
              value={number}
              onChange={(e) => setNumber(e.target.value)}
            />
          </FormControl>

          <Spacer h="3" />

          <FormControl id="password">
            <FormLabel
              htmlFor="password"
              color="customBlue.600"
              fontWeight="semibold"
            >
              Password
            </FormLabel>
            <Input
              id="password"
              type="password"
              placeholder="************"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
          </FormControl>

          <Spacer h="3" />

          <FormControl id="confirmPassword">
            <FormLabel
              htmlFor="confirmPassword"
              color="customBlue.600"
              fontWeight="semibold"
            >
              Confirm Password
            </FormLabel>
            <Input
              id="confirmPassword"
              type="password"
              placeholder="************"
              value={confirmPassword}
              onChange={(e) => setConfirmPassword(e.target.value)}
            />
          </FormControl>

          <Button
            _hover={{ backgroundColor: "customBlue.200", color: "black" }}
            type="submit"
            bgColor="customBlue.600"
            color="white"
            mt="6"
            isLoading={loading}
          >
            Register
          </Button>
        </form>

        <Flex pt="10">
          <Text color="customBlue.600" fontWeight="semibold">
            Already a Customer?{" "}
            <Link as={RouterLink} to="/login">
              Click here to login
            </Link>
          </Text>
        </Flex>
      </FormContainer>
    </Flex>
  )
}

export default RegisterScreen
