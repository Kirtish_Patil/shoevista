import React from "react"
import {
  Button,
  Flex,
  FormControl,
  FormLabel,
  Heading,
  Input,
  Select,
  Spacer,
} from "@chakra-ui/react"
import { useState, useEffect } from "react"
import { useDispatch, useSelector } from "react-redux"
import { useNavigate } from "react-router-dom"
import { saveShippingAddress } from "../actions/cartActions"
import FormContainer from "../components/FormContainer"
import countries from "../data/countries"
import CheckoutSteps from "../components/CheckoutSteps"

const ShippingScreen = () => {
  const dispatch = useDispatch()
  const navigate = useNavigate()

  const cart = useSelector((state) => state.cart)
  const { shippingAddress } = cart

  const [address, setAddress] = useState(shippingAddress.address || "")
  const [city, setCity] = useState(shippingAddress.city || "")
  const [postalCode, setPostalCode] = useState(shippingAddress.postalCode || "")
  const [country, setCountry] = useState(shippingAddress.country || "")

  const submitHandler = (e) => {
    e.preventDefault()

    dispatch(saveShippingAddress({ address, city, postalCode, country }))
    navigate("/payment")
  }
  return (
    <Flex
      w="full"
      alignItems="center"
      justifyContent="center"
      py="5rem"
      bgGradient="linear(to-br, customBlue.400,  customBlue.500, customBlue.700)"
    >
      <FormContainer>
        <CheckoutSteps step1 step2 />

        <form onSubmit={submitHandler}>
          {/* Address */}
          <FormControl id="address" isRequired>
            <FormLabel
              htmlFor="address"
              color="customBlue.600"
              fontWeight="semibold"
            >
              Address
            </FormLabel>
            <Input
              id="address"
              type="text"
              placeholder="Your Address"
              value={address}
              onChange={(e) => setAddress(e.target.value)}
            />
          </FormControl>

          <Spacer h="3" />

          {/* City */}
          <FormControl id="city" isRequired>
            <FormLabel
              htmlFor="city"
              color="customBlue.600"
              fontWeight="semibold"
            >
              City
            </FormLabel>
            <Input
              id="city"
              type="text"
              placeholder="Your City"
              value={city}
              onChange={(e) => setCity(e.target.value)}
            />
          </FormControl>

          <Spacer h="3" />

          {/* postalCode */}
          <FormControl id="postalCode" isRequired>
            <FormLabel
              htmlFor="postalCode"
              color="customBlue.600"
              fontWeight="semibold"
            >
              Postal Code
            </FormLabel>
            <Input
              id="postalCode"
              type="text"
              placeholder="Your Postal Code"
              value={postalCode}
              onChange={(e) => setPostalCode(e.target.value)}
            />
          </FormControl>

          <Spacer h="3" />

          {/* Country */}
          <FormControl id="country" isRequired>
            <FormLabel
              htmlFor="country"
              color="customBlue.600"
              fontWeight="semibold"
            >
              Country
            </FormLabel>
            <Select
              value={country}
              onChange={(e) => setCountry(e.target.value)}
              placeholder="Select Option"
            >
              {countries.map((country) => (
                <option key={country} value={country}>
                  {country}
                </option>
              ))}
            </Select>
          </FormControl>

          <Spacer h="3" />

          <Button
            _hover={{ backgroundColor: "customBlue.200", color: "black" }}
            type="submit"
            bgColor="customBlue.600"
            color="white"
            mt="2"
          >
            Continue
          </Button>
        </form>
      </FormContainer>
    </Flex>
  )
}

export default ShippingScreen
