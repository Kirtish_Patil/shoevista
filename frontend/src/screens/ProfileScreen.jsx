import {
  Button,
  Flex,
  FormControl,
  FormLabel,
  Grid,
  Heading,
  Input,
  Spacer,
  Table,
  Thead,
  Tbody,
  Th,
  Tr,
  Td,
  Icon,
} from "@chakra-ui/react"
import { IoWarning } from "react-icons/io5"
import { useEffect, useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import { Link as RouterLink, useNavigate } from "react-router-dom"
import { USER_DETAILS_RESET } from "../constants/userConstants"
import { getUserDetails, updateUserProfile } from "../actions/userActions"
import { listMyOrders } from "../actions/orderActions"
import FormContainer from "../components/FormContainer"
import Message from "../components/Message"
import Loader from "../components/Loader"

const ProfileScreen = () => {
  const dispatch = useDispatch()
  const navigate = useNavigate()

  const [name, setName] = useState("")
  const [email, setEmail] = useState("")
  const [password, setPassword] = useState("")
  const [confirmPassword, setConfirmPassword] = useState("")
  const [message, setMessage] = useState("")

  const userDetails = useSelector((state) => state.userDetails)
  const { loading, error, user } = userDetails

  const userLogin = useSelector((state) => state.userLogin)
  const { userInfo } = userLogin

  const userUpdateProfile = useSelector((state) => state.userUpdateProfile)
  const { success, user: updatedUser } = userUpdateProfile

  const orderMyList = useSelector((state) => state.orderMyList)
  const { loading: loadingOrders, error: errorOrders, myorders } = orderMyList

  useEffect(() => {

    if (!userInfo) {
      navigate("/login")
    } else {
      if (!user.name ) {
        dispatch(getUserDetails())
        console.log('i ran userDetails')
      } else {
        setName(updatedUser.name || user.name)
        setEmail(updatedUser.email || user.email)
        console.log('i ran setName')
      }
      dispatch(listMyOrders())
    }
  }, [updatedUser, dispatch, navigate, userInfo, success])

  const submitHandler = (e) => {
    e.preventDefault()

    if (password !== confirmPassword) {
      setMessage("Passwords do not match")
    } else {
      dispatch(updateUserProfile({ id: user._id, name, email, password }))
      dispatch({ type: USER_DETAILS_RESET })
    }
  }

  return (
    <Grid
      templateColumns={{ sm: "1fr", md: "1fr 1fr" }}
      py="10"
      px="10"
      gap="10"
      bgGradient="linear(to-br, customBlue.400, customBlue.700)"
    >
      {/* Column 1 */}
      <Flex w="full" alignItems="baseline" justifyContent="center" py="5">
        <FormContainer>
          <Heading as="h1" mb="8" fontSize="3xl" color="customBlue.600">
            User Profile
          </Heading>

          {error && <Message type="error">{error}</Message>}
          {message && <Message type="error">{message}</Message>}

          <form onSubmit={submitHandler}>
            <FormControl id="name">
              <FormLabel
                htmlFor="name"
                color="customBlue.600"
                fontWeight="semibold"
              >
                Your Name
              </FormLabel>
              <Input
                id="name"
                type="text"
                placeholder="Your full name"
                value={name}
                onChange={(e) => setName(e.target.value)}
              />
            </FormControl>

            <Spacer h="3" />

            <FormControl id="email">
              <FormLabel
                htmlFor="email"
                color="customBlue.600"
                fontWeight="semibold"
              >
                Email address
              </FormLabel>
              <Input
                id="email"
                type="email"
                placeholder="username@domain.com"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
            </FormControl>

            <Spacer h="3" />

            <FormControl id="password">
              <FormLabel
                htmlFor="password"
                color="customBlue.600"
                fontWeight="semibold"
              >
                Password
              </FormLabel>
              <Input
                id="password"
                type="password"
                placeholder="************"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
            </FormControl>

            <Spacer h="3" />

            <FormControl id="confirmPassword">
              <FormLabel
                htmlFor="confirmPassword"
                color="customBlue.600"
                fontWeight="semibold"
              >
                Confirm Password
              </FormLabel>
              <Input
                id="confirmPassword"
                type="password"
                placeholder="************"
                value={confirmPassword}
                onChange={(e) => setConfirmPassword(e.target.value)}
              />
            </FormControl>

            <Button
              _hover={{ backgroundColor: "customBlue.200", color: "black" }}
              type="submit"
              bgColor="customBlue.600"
              color="white"
              mt="6"
              isLoading={loading}
            >
              Update
            </Button>
          </form>
        </FormContainer>
      </Flex>

      {/* Column 2 */}
      <Flex direction="column">
        <Heading as="h2" mb="4" color='white'>
          My Orders
        </Heading>

        {loadingOrders ? (
          <Loader />
        ) : errorOrders ? (
          <Message type="error">{errorOrders}</Message>
        ) : (
          <Table variant="simple" bgColor='white' size='md' colorScheme="facebook" borderRadius='xl' fontSize='sm' >
            <Thead >
              <Tr>
                <Th>ID</Th>
                <Th>DATE</Th>
                <Th>TOTAL</Th>
                <Th>PAID</Th>
                <Th>DELIVERED</Th>
                <Th></Th>
              </Tr>
            </Thead>
            <Tbody>
              {myorders.map((order) => (
                <Tr key={order._id}>
                  <Td>{order._id}</Td>
                  <Td>{new Date(order.createdAt).toDateString()}</Td>
                  <Td>₹{order.totalPrice}</Td>
                  <Td>
                    {order.isPaid ? (
                      new Date(order.paidAt).toDateString()
                    ) : (
                      <Icon as={IoWarning} color="red" />
                    )}{" "}
                  </Td>
                  <Td>
                    {order.isDelivered ? (
                      new Date(order.deliveredAt).toDateString()
                    ) : (
                      <Icon as={IoWarning} color="red" />
                    )}{" "}
                  </Td>
                  <Td>
                    <Button
                      as={RouterLink}
                      to={`/order/${order._id}`}
                      colorScheme="teal"
                      size="sm"
                    >
                      Details
                    </Button>
                  </Td>
                </Tr>
              ))}
            </Tbody>
          </Table>
        )}
      </Flex>
    </Grid>
  )
}

export default ProfileScreen
