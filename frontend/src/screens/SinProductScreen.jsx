import {
  Grid,
  Heading,
  Text,
  Flex,
  Button,
  Image,
  Select,
} from "@chakra-ui/react"
import { Link as RouterLink, useNavigate, useParams } from "react-router-dom"
import { useEffect, useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import { listProductDetails } from "../actions/productActions"
import Rating from "../components/Rating"
import Loader from "../components/Loader"
import Message from "../components/Message"

const SinProductScreen = () => {
  const dispatch = useDispatch()
  const navigate = useNavigate()
  const { id } = useParams()

  const [qty, setQty] = useState(1)

  const productDetails = useSelector((state) => state.productDetails)
  const { loading, error, product } = productDetails

  useEffect(() => {
    dispatch(listProductDetails(id))
  }, [id, dispatch])

  const addToCartHandler = () => {
    navigate(`/cart/${id}?qty=${qty}`)
  }
  // console.log(qty)

  const qtyOfItems = [...Array(product.countInStock).keys()]

  return (
    <Flex p='6'>
      <Flex>
        <Button as={RouterLink} to="/">
          Go Back
        </Button>
      </Flex>
      {loading ? (
        <Loader />
      ) : error ? (
        <Message type:error>{error}</Message>
      ) : (
        <Grid
          templateColumns="4fr 3fr"
          display={{ base: "flex", md: "grid" }}
          flexDirection={{ base: "column", md: "row" }}
          w='60%'
          py='10'
          marginX='auto'
          marginY='auto'
        >
          {/* Column 1 */}
          <Image src={product.image} alt={product.name} borderRadius="md" />

          {/* Column 2 */}
          <Flex direction="column">
            <Heading as="h5" fontSize="base" color="gray.500">
              {product.brandName}
            </Heading>

            <Heading as="h2" fontSize="4xl">
              {product.name}
            </Heading>

            <Rating
              value={product.rating}
              text={`${product.numberOfReviews} reviews`}
            />

            <Heading
              as="h5"
              fontSize="4xl"
              fontWeight="bold"
              color="teal.600"
              my="5"
            >
              ${product.price}
            </Heading>

            <Text>{product.description}</Text>

            <Flex direction="column">
              <Flex justifyContent="space-between" py="2">
                <Text>Price:</Text>
                <Text fontWeight="bold">{product.price}</Text>
              </Flex>

              <Flex justifyContent="space-between" py="2" mb="10">
                <Text>Status:</Text>
                <Text fontWeight="bold">
                  {product.countInStock > 0 ? "In Stock" : "Not available"}
                </Text>
              </Flex>

              {product.countInStock > 0 && (
                <Flex justifyContent="space-between" py="2">
                  <Text>Qty:</Text>
                  <Select
                    value={qty}
                    onChange={(e) => setQty(e.target.value)}
                    width="30%"
                  >
                    {qtyOfItems.map((i) => (
                      <option key={(i = 1)}>{i + 1}</option>
                    ))}
                  </Select>
                </Flex>
              )}

              <Button
                bgColor="yellow.500"
                color="gray.800"
                my="2"
                textTransform="uppercase"
                letterSpacing="wide"
                isDisabled={product.countInStock === 0}
                onClick={addToCartHandler}
              >
                Add to Cart
              </Button>
            </Flex>
          </Flex>

          {/* Column 3 */}
        </Grid>
      )}
    </Flex>
  )
}

export default SinProductScreen
