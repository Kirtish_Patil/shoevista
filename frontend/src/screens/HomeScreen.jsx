import React, { useEffect, useState } from "react"
import {
  Heading,
  Flex,
  Grid,
  Spacer,
  Box,
  Breadcrumb,
  BreadcrumbItem,
} from "@chakra-ui/react"
import ProductCard from "../components/ProductCard"
import { useDispatch, useSelector } from "react-redux"
import { listProducts } from "../actions/productActions"
import Loader from "../components/Loader"
import Message from "../components/Message"
import Category from "../components/Category"

const HomeScreen = () => {
  const dispatch = useDispatch()

  const productList = useSelector((state) => state.productList)
  const { loading, error, products } = productList

  useEffect(() => {
    dispatch(listProducts())
  }, [dispatch])

  return (
    <Flex justifyContent="center" direction="column" mx='6' p='6'>
      <Flex display="block" w="100%" >
        {loading ? (
          <Loader />
        ) : error ? (
          <Message type="error">{error}</Message>
        ) : (
          <>
            <Flex
              alignItems="center"
              position="fixed"
              zIndex="5"
              justifyContent="center"
              w="100%"
              pr="7rem"
            >
              <Box>
                <Breadcrumb separator="">
                  <BreadcrumbItem>
                    <Category element={"men"} text={"Men"} />
                  </BreadcrumbItem>
                  <BreadcrumbItem>
                    <Category element={"women"} text={"Women"} />
                  </BreadcrumbItem>
                  <BreadcrumbItem>
                    <Category element={"kids"} text={"Kids"} />
                  </BreadcrumbItem>
                </Breadcrumb>
              </Box>
            </Flex>
            <Spacer py="5" />
            <Heading as="h2" mt={{ base: "10", md: "0" }} >
              Latest Products
            </Heading>

            <Heading
              as="h3"
              id="men"
              fontSize="2xl"
              fontWeight="normal"
              py="6"
              px="4"
            >
              Men's Fashion
            </Heading>
            <Grid
              templateColumns={{ base: "1fr", md: "1fr 1fr 1fr 1fr" }}
              gap="8"
            >
              {products.map((prod) => {
                if (prod.category.includes("Men")) {
                  return <ProductCard product={prod} key={prod._id} />
                }
              })}
            </Grid>

            <Spacer py="5" />

            <Heading
              as="h3"
              id="women"
              fontSize="2xl"
              fontWeight="normal"
              py="6"
              px="4"
              
            >
              Women's Fashion
            </Heading>

            <Grid
              templateColumns={{ base: "1fr", md: "1fr 1fr 1fr 1fr" }}
              gap="8"
            >
              {products.map((prod) => {
                if (prod.category.includes("Women")) {
                  return <ProductCard product={prod} key={prod._id} />
                }
              })}
            </Grid>

            <Spacer py="5" />

            <Heading
              as="h3"
              id="kids"
              fontSize="2xl"
              fontWeight="normal"
              py="6"
              px="4"
              
            >
              Kids's Fashion
            </Heading>

            <Grid
              templateColumns={{ base: "1fr", md: "1fr 1fr 1fr 1fr" }}
              gap="8"
            >
              {products.map((prod) => {
                if (prod.category.includes("Kids")) {
                  return <ProductCard product={prod} key={prod._id} />
                }
              })}
            </Grid>
          </>
        )}
      </Flex>
    </Flex>
  )
}

export default HomeScreen
