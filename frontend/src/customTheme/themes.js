import { extendTheme } from "@chakra-ui/react"
// import "typeface-poppins"
// import "typeface-mukta"
import "typeface-montserrat"
// import '@fontsource/poppins'

const fonts = {
  heading: `montserrat`,
  body: `montserrat`,
}

const colors = {
  brandYellow: {
    600: "#e2bb6c",
  },
  brandBlue: {
    700: "rgba(30, 47, 63, 1)",
    600: "rgb(38,51,62)",
    500: "rgba(38, 51, 62, 0.85)",
    400: "rgba(38, 51, 62, 0.66)",
    300: "rgba(98, 133, 163, 1)",
    200: "rgba(101, 150, 193, 1)",
    50: "rgba(227, 242, 255, 1)",
  },
  customBlue: {
    50: "#EBF7FF",
    100: "#C2E7FF",
    200: "#99D6FF",
    300: "#5CBEFF",
    400: "#1FA5FF",
    500: "#0093F5",
    600: "#006EB8",
    700: "#023e8a",
  },
}

const theme = extendTheme({ colors, fonts })

export default theme
