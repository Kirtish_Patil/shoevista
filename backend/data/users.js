import bcrypt from 'bcryptjs'

const users = [
  {
    name: "Admin",
    email: "admin@example.com",
    number: "1234567890",
    password: bcrypt.hashSync('123456', 10),
    isAdmin: true,
  },
  {
    name: "John Doe",
    email: "john@example.com",
    number: "1234567890",
    password: bcrypt.hashSync('123456', 10),
    isAdmin: false,
  },
  {
    name: "Jane Doe",
    email: "jane@example.com",
    number: "1234567890",
    password: bcrypt.hashSync('123456', 10),
    isAdmin: false,
  },
]

export default users