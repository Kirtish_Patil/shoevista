import asyncHandler from "express-async-handler"
import Product from "../models/productModel.js"

/**
 * @desc    To get all products
 * @route   Get /api/products
 * @access  public
 */

const getProducts = asyncHandler(async (req, res) => {
  const products = await Product.find({})
  res.json(products)
})

/**
 * @desc    Get single product
 * @route   /api/products/:id
 * @access  public
 */

const getProductById = asyncHandler(async (req, res) => {
  const product = await Product.findById(req.params.id)
  if (product) {
    res.json(product)
  } else {
    res.status(404).json({ message: "Product not found" })
  }
})

/**
 * @desc		Delete a product
 * @route		DELETE /api/products/:id
 * @access	private/admin
 */

const deleteProduct = asyncHandler(async (req, res) => {
  const product = await Product.findById(req.params.id)

  if (product) {
    await Product.deleteOne(product)
    res.json({ message: "Product deleted" })
  } else {
    res.status(404)
    throw new Error("Product not found")
  }
})

/**
 * @desc    Create a product
 * @route   Post /api/products
 * @access  public
 */

const createProduct = asyncHandler(async (req, res) => {
  const product = new Product({
    user: req.user._id,
    name: "Sample product",
    image: "/image/sample.jpg",
    description: "Sample Description",
    brandName: "Sample Brand",
    category: "Sample Category",
    price: 0,
    countInStock: 0,
    numReviews: 0,
  })

  const createdProduct = await product.save()
  res.status(201).json(createdProduct)
})

/**
 * @desc    Update a product
 * @route   Put /api/products/:id
 * @access  public/admin
 */

const updateProduct = asyncHandler(async (req, res) => {
  const { name, price, description, image, brandName, category, countInStock } =
    req.body

  const product = await Product.findById(req.params.id)

  if (product) {
    product.name = name
    product.price = price
    product.description = description
    product.image = image
    product.category = category
    product.countInStock = countInStock

    const updatedProduct = await product.save()
    res.json(updatedProduct)
  } else {
    res.status(404)
    throw new Error("Product not found")
  }
})

export {
  getProducts,
  getProductById,
  createProduct,
  updateProduct,
  deleteProduct,
}
