import mongoose from 'mongoose'

const connectDB = async() => {
    try{
        const conn = await mongoose.connect(process.env.MONGO_URI)
        console.log(`Mongoose connected: ${conn.connection.host}`.yellow)
    } catch (err) {
        console.error(`[mongo]: ${err.message}`.red.bold)
        process.exit(1)
    }
}

export default connectDB